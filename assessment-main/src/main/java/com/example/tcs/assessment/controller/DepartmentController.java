package com.example.tcs.assessment.controller;

import com.example.tcs.assessment.entity.Department;
import com.example.tcs.assessment.entity.Employee;
import com.example.tcs.assessment.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DepartmentController {

    @Autowired
    private IDepartmentService departmentService;

    /**
     * Metodo para buscar todos los departamentos.
     * @return lista de todos los departamentos
     */
    @GetMapping("/allDepartments")
    public ResponseEntity<List<Department>> getAllDepartments() {

        return departmentService.getAllDepartments();
    }

    /**
     * Obtener un departamento.
     * @param id del depto
     * @return el depto buscado
     */
    @GetMapping("/department/id/{id}")
    public ResponseEntity<Department> getDepartmentById(@PathVariable("id") int id) {
        return departmentService.getDepartment(id);
    }

    /**
     * Metodo para almacenar un depto en bd.
     * @param department el objeto a alamcenar
     * @return el objeto almacenado
     */
    @PostMapping("/department")
    public ResponseEntity<Department> saveDepartment(@RequestBody Department department) {
        return departmentService.saveDepartment(department);
    }


    /**
     * Metodo para actualiazar un depto.
     * @param id del depto a actualizar
     * @param department el objeto con los nuevos valores
     * @return el objeto actualizado
     */
    @PutMapping("/department/{id}")
    public ResponseEntity<Department> updateDepartment(@PathVariable("id") int id, @RequestBody Department department) {
        return departmentService.updateDepartment(id, department);
    }

    /**
     * Metodo para eliminar un departamento.
     * @param id del depto.
     * @return leyenda y estatus.
     */
    @DeleteMapping("/department/{id}")
    public ResponseEntity<String> deleteDepartment(@PathVariable("id") int id) {
        return departmentService.deleteDepartment(id);
    }
}
