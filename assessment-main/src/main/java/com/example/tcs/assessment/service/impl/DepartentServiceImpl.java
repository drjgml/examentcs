package com.example.tcs.assessment.service.impl;

import com.example.tcs.assessment.entity.Department;
import com.example.tcs.assessment.entity.Employee;
import com.example.tcs.assessment.repository.DepartmentRepository;
import com.example.tcs.assessment.repository.EmployeeRepository;
import com.example.tcs.assessment.service.IDepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class DepartentServiceImpl implements IDepartmentService {
    @Autowired
    private DepartmentRepository departmentRepository;

    /**
     * Metodo para buscar todos los departamentos.
     * @return lista de todos los departamentos
     */
    @Override
    public ResponseEntity<List<Department>> getAllDepartments() {
        log.info("Iniciando busqueda de empledos");
        try {
            List<Department> departmentList = departmentRepository.findAll();
            if (departmentList != null) {
                log.info("Busqueda de empleados exitosa");
                return new ResponseEntity<List<Department>>(departmentList, HttpStatus.OK);
            } else {
                log.info("No se ha podido realizar la busqueda de empleados");
                return new ResponseEntity<List<Department>>(departmentList, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.info("Ocurrio un error al consultar departamentos:  " + e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Metodo para buscar un departamento.
     * @param id del departamento
     * @return el departamento buscado
     */
    @Override
    public ResponseEntity<Department> getDepartment(int id) {
        try {
            log.info("Iniciando busqueda de departamento id: " + id);
            Optional<Department> departmentOpt = departmentRepository.findById(id);
            if (departmentOpt.isPresent()) {
                log.info("Se realizó busqueda de manera exitosa");
                return new ResponseEntity<Department>(departmentOpt.get(), HttpStatus.OK);
            } else {
                log.info("No se pudo realizar la busqueda");
                return new ResponseEntity<Department>(new Department(), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.info("Ocurrio un error al intentar consultar el empleado");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Metodo para almacenar un departamento en bd.
     * @param department el objeto a almacenar
     * @return el objeto almacenado
     */
    @Override
    public ResponseEntity<Department> saveDepartment(Department department) {
        log.info("Almacenando departamento {}", department.toString());
        try {
            Optional<Department> departmentOpt = Optional.of(departmentRepository.save(department));
            if (departmentOpt.isPresent()) {
                log.info("Departamento almacenado");
                return new ResponseEntity<>(departmentOpt.get(), HttpStatus.OK);
            } else {
                log.info("No se ha podido almacenar el departamento");
                return new ResponseEntity<>(new Department(), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.info("Ocurrio un error al intentar almacenar el departamento");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Metodo para actualizar un departamento.
     * @param id del depto a actualizar
     * @param department el objeto con los datos a actualizar
     * @return el objeto actualizado
     */
    @Override
    public ResponseEntity<Department> updateDepartment(int id, Department department) {
        log.info("Iniciando update del departamento: " + department.toString());
        Department departmentAux = getDepartment(id).getBody();
        try {
            departmentAux.setName(department.getName());
            departmentAux.setAddress(department.getAddress());

            departmentAux = departmentRepository.save(departmentAux);
            return new ResponseEntity<>(departmentAux, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Ocurrio un error al intentar actualizar el registro: " + e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Metodo para eliminar un deparatamento.
     * @param id del departamento a eliminar
     * @return leyenda y estatus
     */
    @Override
    public ResponseEntity<String> deleteDepartment(int id) {
        log.info("Se procederá a eliminar departamento con id: " + id);
        try {
            departmentRepository.deleteById(id);
            log.info("Departamento eliminado con exito");
            return new ResponseEntity<String>("Departamento eliminado con exito", HttpStatus.OK);
        } catch (Exception e) {
            log.info("Ocurrio un error al intentar eliminar el deparatamento");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
}
