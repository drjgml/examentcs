package com.example.tcs.assessment.repository;

import com.example.tcs.assessment.entity.Department;
import com.example.tcs.assessment.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<Department, Integer> {
}
