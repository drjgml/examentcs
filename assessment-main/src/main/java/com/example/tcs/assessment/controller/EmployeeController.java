package com.example.tcs.assessment.controller;

import com.example.tcs.assessment.entity.Employee;
import com.example.tcs.assessment.service.IEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmployeeController {

    @Autowired
    private IEmployeeService employeeService;

    /**
     * Metodo para obtener todos los empleados.
     *
     * @return el response con el total de emleados.
     */
    @GetMapping("/allEmployees")
    public ResponseEntity<List<Employee>> getAllEmpleados() {

        return employeeService.getAllEmployee();
    }

    @GetMapping("/employee/id/{id}")
    public ResponseEntity<Employee> getEmpledoPorId(@PathVariable("id") int id) {
        return employeeService.getEmployee(id);
    }

    /**
     * Metodo para almacenar un empleado.
     *
     * @param employee Objeto a almacenar en bd
     * @return leyenda y estatus
     */
    @PostMapping("/employee")
    public ResponseEntity<Employee> saveEmpleado(@RequestBody Employee employee) {
        return employeeService.saveEmployee(employee);
    }

    /**
     * Metodo para actualizar datos de un empleado.
     *
     * @param id       el id del emplead a modificar.
     * @param employee el objeto con los nuevos datos por actualizar
     * @return leyenda y estatus
     */
    @PutMapping("/employee/{id}")
    public ResponseEntity<Employee> updateEmpleado(@PathVariable("id") int id, @RequestBody Employee employee) {
        return employeeService.updateEmployee(id, employee);
    }

    /**
     * Metodo para borrar un registro.
     *
     * @param id el id del registro a eliminar.
     * @return estatus.
     */
    @DeleteMapping("/employee/{id}")
    public ResponseEntity<String> deleteEmpleado(@PathVariable("id") int id) {
        return employeeService.deleteEmployee(id);
    }
}
