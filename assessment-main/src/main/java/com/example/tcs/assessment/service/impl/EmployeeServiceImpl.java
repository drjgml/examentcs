package com.example.tcs.assessment.service.impl;

import com.example.tcs.assessment.entity.Employee;
import com.example.tcs.assessment.repository.EmployeeRepository;
import com.example.tcs.assessment.service.IEmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class EmployeeServiceImpl implements IEmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    /**
     * Metodo para retornar todos los empleados
     *
     * @return Lista de todos los empleados
     */
    @Override
    public ResponseEntity<List<Employee>> getAllEmployee() {
        log.info("Iniciando busqueda de empledos");
        try {
            List<Employee> empleadosList = employeeRepository.findAll();
            if (empleadosList != null) {
                log.info("Busqueda de empleados exitosa");
                return new ResponseEntity<List<Employee>>(empleadosList, HttpStatus.OK);
            } else {
                log.info("No se ha podido realizar la busqueda de empleados");
                return new ResponseEntity<List<Employee>>(empleadosList, HttpStatus.OK);
            }

        } catch (Exception e) {
            log.info("Ocurrio una Exception al consultar empleados:  " + e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Metodo para buscar solo un empleado
     *
     * @param id el id del empleado a buscar
     * @return el empleado y estatus
     */
    public ResponseEntity<Employee> getEmployee(int id) {
        try {
            log.info("Iniciando busqueda de empleado id: " + id);
            Optional<Employee> employeeOpt = employeeRepository.findById(id);
            if (employeeOpt.isPresent()) {
                log.info("Se realizó busqueda de manera exitosa");
                return new ResponseEntity<Employee>(employeeOpt.get(), HttpStatus.OK);
            } else {
                log.info("No se pudo realizar la busqueda");
                return new ResponseEntity<Employee>(new Employee(), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.info("Ocurrio un error al intentar consultar el empleado");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Metodo para almacenar un nuevo empleado
     *
     * @param employee empleado a almacenar
     * @return el empleado almacenado
     */
    @Override
    public ResponseEntity<Employee> saveEmployee(Employee employee) {
        log.info("Almacenando empleado {}", employee.toString());
        try {
            Optional<Employee> employeeOpt = Optional.of(employeeRepository.save(employee));
            if (employeeOpt.isPresent()) {
                log.info("Empleado almacenado");
                return new ResponseEntity<>(employeeOpt.get(), HttpStatus.OK);
            } else {
                log.info("No se ha podido almacenar el empleado");
                return new ResponseEntity<>(new Employee(), HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            log.info("Ocurrio un error al inetntar almacenar el empleado");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<Employee> updateEmployee(int id, Employee employee) {
        log.info("Iniciando update del empleado: " + employee.toString());
        Employee employeeAux = getEmployee(id).getBody();
        try {
            employeeAux.setEmployeeNumber(employee.getEmployeeNumber());
            employeeAux.setAccountNumber(employee.getAccountNumber());
            employeeAux.setFirstName(employee.getFirstName());
            employeeAux.setLastName(employee.getLastName());
            employeeAux.setAge(employee.getAge());
            employeeAux.setDepartment(employee.getDepartment());

            employeeAux = employeeRepository.save(employeeAux);
            return new ResponseEntity<>(employeeAux, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Ocurrio un error al intentar actualizar el registro: " + e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<String> deleteEmployee(int id) {
        log.info("Se procederá a eliminar empleado con id: " + id);
        try {
            employeeRepository.deleteById(id);
            log.info("Empleado eliminado con exito");
            return new ResponseEntity<String>("Empleado eliminado con exito", HttpStatus.OK);
        } catch (Exception e) {
            log.info("Ocurrio un error al intentar eliminar el empleado");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }
}
