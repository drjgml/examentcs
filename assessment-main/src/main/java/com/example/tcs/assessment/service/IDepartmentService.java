package com.example.tcs.assessment.service;

import com.example.tcs.assessment.entity.Department;
import com.example.tcs.assessment.entity.Employee;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IDepartmentService {
    public ResponseEntity<List<Department>> getAllDepartments();

    public ResponseEntity<Department> getDepartment(int id);

    public ResponseEntity<Department> saveDepartment(Department department);

    public ResponseEntity<Department> updateDepartment(int id, Department department);

    public ResponseEntity<String> deleteDepartment(int id);
}
