package com.example.tcs.assessment.service;

import com.example.tcs.assessment.entity.Employee;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IEmployeeService {
    public ResponseEntity<List<Employee>> getAllEmployee();

    public ResponseEntity<Employee> getEmployee(int id);

    public ResponseEntity<Employee> saveEmployee(Employee employee);

    public ResponseEntity<Employee> updateEmployee(int id, Employee employee);

    public ResponseEntity<String> deleteEmployee(int id);
}
